# Spring Boot Template Project

## Thing to do list:
1. Clone this repository: `https://gitlab.com/hendisantika/spring-boot-template-project.git`
2. Go to your folder: `spring-boot-template-project`
3. Run the application: `mvn clean spring-boot:run`
