package com.hendisantika.springboottemplateproject.repository;

import com.hendisantika.springboottemplateproject.entity.Book;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-template-project
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 20/12/19
 * Time: 06.12
 */
@Repository
public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
}